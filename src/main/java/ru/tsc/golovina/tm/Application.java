package ru.tsc.golovina.tm;

import ru.tsc.golovina.tm.constant.ArgumentConst;
import ru.tsc.golovina.tm.constant.TerminalConst;
import ru.tsc.golovina.tm.model.Command;
import ru.tsc.golovina.tm.repository.CommandRepository;
import ru.tsc.golovina.tm.util.NumberUtil;

import java.util.Arrays;
import java.util.Scanner;

public class Application {

    private static final CommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(String[] args) {
        displayWelcome();
        parseArgs(args);
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = scanner.nextLine();
            parseCommand(command);
        }
    }

    private static void displayWelcome() {
        System.out.println("---Welcome to task manager---");
    }

    public static void parseArgs(String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArg(arg);
        exit(0);
    }

    public static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            default:
                showArgumentError();
                exit(1);
        }
    }

    public static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.EXIT:
                exit(0);
                break;
            default:
                showCommandError();
        }
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands)
            System.out.println(command);
    }

    public static void showCommands() {
        System.out.println("[COMMANDS]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands)
            showCommandValue(command.getName());
    }

    public static void showArguments() {
        System.out.println("[ARGUMENTS]");
        final Command[] commands = COMMAND_REPOSITORY.getCommands();
        for (final Command command : commands)
            showCommandValue(command.getArgument());

    }

    private static void showCommandValue(final String value) {
        if (value == null || value.isEmpty())
            return;
        System.out.println(value);
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.0");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer: Alla Golovina");
        System.out.println("e-mail: amedvedeva@tsconsulting.com");
    }

    public static void showInfo() {
        System.out.println("[INFO]");
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory: " + NumberUtil.formatBytes(totalMemory));
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("Used memory: " + NumberUtil.formatBytes(usedMemory));
    }

    public static void showArgumentError() {
        System.err.println("Error! Argument not found...");
    }

    public static void showCommandError() {
        System.err.println("Error! Command not found...");
    }

    public static void exit(int status) {
        System.exit(status);
    }

}